import os
import platform
env = Environment(ENV = os.environ)

# Build Options
debug_build = ARGUMENTS.get('debug', 0)

# enable choosing other compilers
env["CC"] = os.getenv("CC") or env["CC"]
env["CXX"] = os.getenv("CXX") or env["CXX"]
env["ENV"].update(x for x in os.environ.items() if x[0].startswith("CCC_"))

resbld = Builder(action = 'rc -o $TARGET $SOURCE')
env.Append(BUILDERS = {'Resource' : resbld})
env.Append(CPPPATH = ['#/src'])

# Add a dependency from pkg-config
def CheckPKGConfig(context, version):
	context.Message( 'Checking for pkg-config... ' )
	ret = context.TryAction('pkg-config --atleast-pkgconfig-version=%s' % version)[0]
	context.Result( ret )
	return ret

conf = Configure(env, custom_tests = { 'CheckPKGConfig' : CheckPKGConfig })
if not conf.CheckPKGConfig('0.15.0'):
	print('pkg-config >= 0.15.0 not found.')
	Exit(1)

env = conf.Finish()

if int(debug_build):
    env.Append(CCFLAGS = '-g')
    env.Append(CPPDEFINES = ['DEBUG'])

machineArch = platform.machine()
env["BUILD_DIR"] = '#/target-' + machineArch

Export("env")
SConscript('src/SConscript', variant_dir=env["BUILD_DIR"], duplicate=0)
Clean('.', env["BUILD_DIR"])
