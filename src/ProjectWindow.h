/*
 * Copyright 2017, Alexander von Gluck IV <kallisti5@unixzen.com>
 *
 * Released under the terms of the MIT license.
 */
#ifndef _PROJECT_WINDOW_H
#define _PROJECT_WINDOW_H


#include <Entry.h>
#include <Window.h>
#include <Message.h>
#include <ListItem.h>
#include <OutlineListView.h>
#include <String.h>


class ProjectItem : public BListItem {
public:
							ProjectItem(const char* label);
							~ProjectItem();

	virtual void			DrawItem(BView* owner, BRect item_rect, bool complete);

private:
			BString			fItemText;
			BFont			fBoldFont;
			font_height		fBoldFontHeight;
};


class ProjectListView : public BOutlineListView {
public:
							ProjectListView();
							~ProjectListView();
};



class ProjectWindow : public BWindow {
public:
							ProjectWindow(BRect frame, const char* title);
							~ProjectWindow();

			void			Open(entry_ref* ref);

//	virtual bool			QuitRequested();
//	virtual void			MessageReceived(BMessage* message);
private:
			ProjectListView* fProjectListView;
};


#endif /* _PROJECT_WINDOW_H */
