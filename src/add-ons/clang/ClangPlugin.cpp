/*
 * Copyright, 2017-2018 Alexander von Gluck IV. All rights reserved.
 * Released under the terms of the MIT license
 */


#include <clang-c/Index.h>
#include <stdio.h>

#include "MicrobeAddOn.h"


class ClangPlugin : public MicrobeAddOn {
public:
							ClangPlugin(EditorContext* context);
							~ClangPlugin();
		void				Updated();

protected:
		CXIndex				fIndex;
		CXTranslationUnit	fTu;
}; 


ClangPlugin::ClangPlugin(EditorContext* context)
	:
	MicrobeAddOn(context),
	fIndex(NULL),
	fTu(NULL)
{
	fIndex = clang_createIndex(0, 0);
}


ClangPlugin::~ClangPlugin()
{
	clang_disposeIndex(fIndex);
}


void
ClangPlugin::Updated()
{
	printf("plugin: clang received your change!\n");
}


//int main(int argc, char* argv[]) {
//	CXTranslationUnit tu = clang_parseTranslationUnit(index, 0,
//		argv, argc, 0, 0, CXTranslationUnit_None);
//	for (unsigned i = 0, n = clang_getNumDiagnostics(tu); i != n; i++) {
//		CXDiagnostic diag = clang_getDiagnostic(tu, i);
//		CXString string = clang_formatDiagnostic(diag,
//			clang_defaultDiagnosticDisplayOptions());
//		fprintf(stderr, "%s\n", clang_getCString(string));
//		clang_disposeString(string);
//	}
//	clang_disposeTranslationUnit(tu);
//	return 0;
//}


// #pragma mark -


extern "C"
MicrobeAddOn*
instantiate_microbe_add_on(EditorContext* context)
{
    return new ClangPlugin(context);
}
