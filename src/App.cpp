/*
 * Copyright 2011, Alexandre Deckner (alex@zappotek.com)
 * Distributed under the terms of the MIT License.
 *
 */


#include "App.h"

#include <Alert.h>
#include <Entry.h>
#include <String.h>

#include "MainWindow.h"
#include "ProjectWindow.h"
#include "GlobalMessage.h"


const char* kSignature = "application/x-vnd.Uzzl-Microbe";


App::App()
	:
	BApplication(kSignature)
{
	BRect frame(50, 50, 640 + 50, 480 + 50);
	const char *title = "Microbe";
	fMainWindow = new MainWindow(frame, title);
	fProjectWindow = new ProjectWindow(BRect(50,50,200,400), "Project");
}


App::~App()
{
}


void App::AboutRequested()
{
	if (fMainWindow) {
		fMainWindow->AboutRequested();
	}
}


void App::MessageReceived(BMessage *message)
{
	switch(message->what) {
		case kMsgOpenFolder:
			if (fProjectWindow) {
				entry_ref ref;
				message->FindRef("prefix", &ref);
				fProjectWindow->Open(&ref);
			}
			return;
	}
	BApplication::MessageReceived(message);
}


int
main(int argc, char** argv)
{
	App app;
	app.Run();
	return 0;
}
