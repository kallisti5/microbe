/*
 * Copyright 2011, Alexandre Deckner (alex@zappotek.com)
 * Distributed under the terms of the MIT License.
 *
 */
#ifndef _MAINWINDOW_H
#define _MAINWINDOW_H


#include <StringView.h>
#include <ScrollView.h>
#include <Window.h>


class MainView;
class BFilePanel;


class MainWindow : public BWindow {
public:
								MainWindow(BRect frame, const char* title);
								~MainWindow();

		void				AboutRequested();
	virtual	bool				QuitRequested();
	virtual	void				MessageReceived(BMessage* message);

private:
			void				_MessageDropped(BMessage* message);
			void				_StatusTextUpdate(BMessage* message);

private:
			MainView*			fMainView;
			BScrollView*			fScrollView;
			BFilePanel*			fOpenFilePanel;
			BFilePanel*			fSaveFilePanel;
			BFilePanel*			fOpenFolderPanel;
			BStringView*		fStatusText;
			BStringView*		fStatusRight;
};


#endif	// _MAINWINDOW_H
