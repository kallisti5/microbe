/*
 * Copyright 2017 Alexander von Gluck IV <kallisti5@unixzen.com>
 *
 * Released under the terms of the MIT license.
 */


#include "ProjectWindow.h"

#include <LayoutBuilder.h>
#include <ListItem.h>


ProjectItem::ProjectItem(const char* title)
	:
	BListItem(),
	fItemText(title)
{
	fBoldFont.SetFace(B_BOLD_FACE);
}


ProjectItem::~ProjectItem()
{

}


void
ProjectItem::DrawItem(BView* owner, BRect item_rect, bool complete)
{
	owner->PushState();

	float width;
	owner->GetPreferredSize(&width, NULL);
	BString text(fItemText);
	owner->SetHighColor(ui_color(B_LIST_ITEM_TEXT_COLOR));
	owner->SetFont(&fBoldFont);
	owner->TruncateString(&text, B_TRUNCATE_END, width);
	owner->DrawString(text.String(), BPoint(item_rect.left,
		item_rect.bottom - fBoldFontHeight.descent));

	owner->PopState();
}


ProjectListView::ProjectListView()
	:
	BOutlineListView("Project List")
{

}


ProjectListView::~ProjectListView()
{

}


ProjectWindow::ProjectWindow(BRect frame, const char* title)
	:
	BWindow(frame, title, B_FLOATING_WINDOW_LOOK, B_NORMAL_WINDOW_FEEL,
		B_AUTO_UPDATE_SIZE_LIMITS),
	fProjectListView(NULL)
{
	fProjectListView = new ProjectListView();
	BLayoutBuilder::Group<>(this, B_HORIZONTAL, B_USE_ITEM_SPACING)
		.Add(fProjectListView)
		.End();
}


ProjectWindow::~ProjectWindow()
{

}


void
ProjectWindow::Open(entry_ref* ref)
{
	ProjectItem* item = new ProjectItem(ref->name);
	fProjectListView->AddItem(item);

	Show();
}
