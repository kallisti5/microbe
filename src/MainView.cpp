/*
 * Copyright 2011, Alexandre Deckner (alex@zappotek.com)
 * Copyright 2017, Alexander von Gluck IV <kallisti5@unixzen.com>
 *
 * Distributed under the terms of the MIT License.
 */


#include "MainView.h"

#include <Directory.h>
#include <Entry.h>
#include <File.h>
#include <Path.h>
#include <PathFinder.h>
#include <Region.h>
#include <String.h>

#include <map>
#include <stdio.h>

#include "GlobalMessage.h"
#include "Highlighter.h"
#include "MicrobeAddOn.h"


MainView::MainView(BRect frame)
	:
	//BTextView("MainView")
	BTextView(frame, "textview", frame,
		B_FOLLOW_ALL, B_FRAME_EVENTS | B_WILL_DRAW)
{
	// Load our compiled plugins
	ReloadAddOns();

	fHighlighter = new Highlighter(this);
	SetStylable(true);
}


MainView::~MainView()
{
}


status_t
MainView::Load(const entry_ref& ref)
{
	status_t status = B_ERROR;

	// TODO: Read only option?
	BFile targetLoad(&ref, B_READ_WRITE);
	if (status = targetLoad.InitCheck() < B_OK)
		return status;

	// Clear our view
	SetText("");

	// Load document 2048 chars at a time
	char buffer[2048];
	off_t length;
	targetLoad.GetSize(&length);

	while (length > 0) {
		ssize_t i = targetLoad.Read(buffer, sizeof(buffer));
		Insert(buffer, i);
		length -= i;
	}

	return B_OK;
}


status_t
MainView::Write(const entry_ref& ref)
{

	return B_ERROR;
}


Highlighter*
MainView::GetHighlighter() const
{
	return fHighlighter;
}


void
MainView::AttachedToWindow()
{
	BView::AttachedToWindow();
	fWindow = Window();
	fContext.view = this;
}


void
MainView::Draw(BRect updateRect)
{
	BTextView::Draw(updateRect);
	fHighlighter->Draw();
}


void
MainView::FrameResized(float width, float height)
{
}


void
MainView::Select(int32 start, int32 finish)
{
	BTextView::Select(start, finish);
}


void
MainView::DeleteText(int32 start, int32 finish)
{
	BTextView::DeleteText(start, finish);

	// Tell our plugins that text was deleted
	for (uint32 i = fAddOns.CountItems(); i-- > 0;) {
		MicrobeAddOn* addOn = fAddOns.ItemAt(i);
		// TODO: Be more efficent!
		addOn->Update();
	}
}


void
MainView::InsertText(const char *text, int32 length, int32 offset,
	const text_run_array *runs)
{
	BMessage* msg = new BMessage(kMsgStatusUpdate);
	msg->AddString("text", "Processing...");
	fWindow->PostMessage(msg);

	rgb_color black = { 0, 0, 0, 255};
	SetFontAndColor(be_fixed_font, B_FONT_ALL, &black);

	BTextView::InsertText(text, length, offset, runs);

	// Tell our plugins that text was deleted
	bigtime_t start = system_time();
	for (uint32 i = fAddOns.CountItems(); i-- > 0;) {
		MicrobeAddOn* addOn = fAddOns.ItemAt(i);
		// TODO: Be more efficent!
		addOn->Update();
	}
	printf("add-on time: %lims\n", (system_time() - start) / 1000);

	msg = new BMessage(kMsgStatusUpdate);
	msg->AddString("text", "Idle.");
	msg->AddUInt32("delay", (system_time() - start) / 1000);
	fWindow->PostMessage(msg);
}


void
MainView::ReloadAddOns()
{
	BStringList paths;
	BPathFinder::FindPaths(B_FIND_PATH_ADD_ONS_DIRECTORY, "microbe", paths);

	typedef std::map<BString, BPath> PathMap;
	PathMap addOnMap;

	for (int32 i = 0; i < paths.CountStrings(); i++) {
		BDirectory directory(paths.StringAt(i));
		BEntry entry;
		while (directory.GetNextEntry(&entry) == B_OK) {
			BPath path;
			if (entry.GetPath(&path) != B_OK)
				continue;

			if (addOnMap.find(path.Leaf()) == addOnMap.end())
				addOnMap.insert(std::pair<BString, BPath>(path.Leaf(), path));
		}
	}
	for (PathMap::const_iterator addOnIterator = addOnMap.begin();
		addOnIterator != addOnMap.end(); addOnIterator++) {
		const BPath& path = addOnIterator->second;

		image_id image = load_add_on(path.Path());
		if (image < 0) {
			printf("Failed to load %s addon: %s.\n", path.Path(),
			strerror(image));
			continue;
		}

		MicrobeAddOn* (*instantiateAddOn)(MainView* view);

		status_t status = get_image_symbol(image, "instantiate_microbe_add_on",
			B_SYMBOL_TYPE_TEXT, (void**)&instantiateAddOn);

		if (status != B_OK) {
			// No "addon instantiate function" symbol found in this addon
			printf("No symbol \"instantiate_microbe_add_on\" found "
				"in %s addon: not a microbe add-on!\n", path.Path());
			unload_add_on(image);
			continue;
		}

		MicrobeAddOn* addOn = instantiateAddOn(this);
		if (addOn == NULL) {
			unload_add_on(image);
			continue;
		}

		fAddOns.AddItem(addOn);
	}

	printf("(Re)loaded %d Add-Ons\n", fAddOns.CountItems());
}
