/*
 * Copyright, 2017-2018 Alexander von Gluck IV. All rights reserved.
 * Released under the terms of the MIT license
 */
#ifndef _MICROBE_ADDON_H
#define _MICROBE_ADDON_H


#include <SupportDefs.h>
#include <image.h>


class MainView;

struct EditorContext {
        int32 lines;
        int32 cursorX;
        int32 cursorY;
        MainView* view;
};

class MicrobeAddOn {
public:
					MicrobeAddOn(EditorContext* context)
						:
						fContext(context) {};
					~MicrobeAddOn() {};

	virtual void	Update() {};

// TODO: This isn't very efficent
//	virtual void	Text(const char* text) {};

//	virtual void	InsertText(const char *text, int32 length, int32 offset) {};
//	virtual void	DeleteText(int32 start, int32 finish) {};
//	virtual void	Select(int32 start, int32 finish) {};

protected:
	EditorContext*	fContext;
};


#endif /* _MICROBE_ADDON_H */
