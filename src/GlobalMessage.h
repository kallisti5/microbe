/*
 * Copyright, 2017 Alexander von Gluck IV. All rights reserved.
 * Released under the terms of the MIT license.
 */
#ifndef _GLOBAL_MESSAGE_H
#define _GLOBAL_MESSAGE_H


const uint32 kMsgStatusUpdate = 'stus';
const uint32 kMsgOpenFolder = 'opf';


#endif /* _GLOBAL_MESSAGE_H */
