/*
 * Copyright 2011, Alexandre Deckner (alex@zappotek.com)
 * Copyright 2017, Alexander von Gluck IV <kallisti5@unixzen.com>
 *
 * Distributed under the terms of the MIT License.
 */


#include "MainWindow.h"

#include <Alert.h>
#include <Application.h>
#include <Entry.h>
#include <FilePanel.h>
#include <LayoutBuilder.h>
#include <MenuBar.h>
#include <MenuItem.h>
#include <be_apps/Tracker/RecentItems.h>

#include <stdio.h>

#include "App.h"
#include "MainView.h"
#include "GlobalMessage.h"


const uint32 kMsgOpenFilePanel = 'opfp';
const uint32 kMsgOpenFolderPanel = 'opfo';
const uint32 kMsgSaveFilePanel = 'svfp';
const uint32 kMsgSaveFile = 'save';
const uint32 kMsgNewFile = 'newf';

const uint32 kMsgAbout = 'abut';


MainWindow::MainWindow(BRect frame, const char* title)
	:
	BWindow(frame, title, B_DOCUMENT_WINDOW_LOOK, B_NORMAL_WINDOW_FEEL,
		B_AUTO_UPDATE_SIZE_LIMITS),
	fOpenFilePanel(NULL),
	fSaveFilePanel(NULL),
	fMainView(NULL),
	fStatusText(NULL),
	fStatusRight(NULL)
{
	fOpenFilePanel = new BFilePanel(B_OPEN_PANEL);
	fOpenFilePanel->SetTarget(this);
	fSaveFilePanel = new BFilePanel(B_SAVE_PANEL);
	fSaveFilePanel->SetTarget(this);
	fOpenFolderPanel = new BFilePanel(B_OPEN_PANEL, NULL, NULL, B_DIRECTORY_NODE);
	fOpenFolderPanel->SetTarget(this);

	BMenuBar* menuBar = new BMenuBar("menu bar");
	BMenu* menu;

	menu = new BMenu("File");
	menu->AddItem(new BMenuItem("New file",
		new BMessage(kMsgNewFile), 'N', B_COMMAND_KEY));
	menu->AddItem(new BMenuItem("Open file" B_UTF8_ELLIPSIS,
		new BMessage(kMsgOpenFilePanel), 'O', B_COMMAND_KEY));
	menu->AddItem(new BMenuItem("Open folder" B_UTF8_ELLIPSIS,
		new BMessage(kMsgOpenFolderPanel), 0, 0));
	menu->AddSeparatorItem();
	menu->AddItem(new BMenuItem("Save",
		new BMessage(kMsgSaveFile), 'S', B_COMMAND_KEY));
	menu->AddItem(new BMenuItem("Save as" B_UTF8_ELLIPSIS,
		new BMessage(kMsgSaveFilePanel), 0, 0));
	menu->AddSeparatorItem();
	menu->AddItem(new BMenuItem("Quit",
		new BMessage(B_QUIT_REQUESTED), 'Q', B_COMMAND_KEY));
	menuBar->AddItem(menu);

	menu = new BMenu("Help");
	menu->AddItem(new BMenuItem("About" B_UTF8_ELLIPSIS,
		new BMessage(kMsgAbout)));
	menuBar->AddItem(menu);

	fMainView = new MainView(Bounds());
	fScrollView = new BScrollView("scrollview", fMainView, B_FOLLOW_ALL,
		0, true, true, B_FANCY_BORDER);

	fStatusRight = new BStringView("performance", "");
	fStatusRight->SetAlignment(B_ALIGN_RIGHT);
	fStatusText = new BStringView("status", "Idle.");
	fStatusText->SetAlignment(B_ALIGN_LEFT);
	fStatusText->SetExplicitMaxSize(BSize(B_SIZE_UNLIMITED, B_SIZE_UNSET));
	fStatusText->SetExplicitMinSize(BSize(150, 12));

	BLayoutBuilder::Group<>(this, B_VERTICAL, 0)
		.Add(menuBar)
		.Add(fScrollView)
		.AddGroup(B_HORIZONTAL)
			.Add(fStatusText)
			.Add(fStatusRight)
			.SetInsets(0, 0, 15, 0)
		.End();
	Show();
}


MainWindow::~MainWindow()
{
}


bool
MainWindow::QuitRequested()
{
	be_app->PostMessage(B_QUIT_REQUESTED);
	return true;
}


void
MainWindow::AboutRequested()
{
	BAlert* alert;
	BString aboutText("Microbe - A programming microorganism\n");
	aboutText.Append("Released under the terms of the MIT license.\n\n");
	if (fMainView) {
		char addonsLoaded[128];
		snprintf(addonsLoaded, 127, "  Functionality enhanced by %d add-on(s).\n\n", fMainView->CountAddOns());
		aboutText.Append(addonsLoaded);
	}
	alert = new BAlert("About", aboutText, "ok");
	alert->Go(NULL);
}

void
MainWindow::MessageReceived(BMessage *message)
{
	switch (message->what) {
		case kMsgOpenFolderPanel:
			fOpenFolderPanel->Show();
			break;
		case kMsgOpenFilePanel:
			fOpenFilePanel->Show();
			break;
		case kMsgSaveFile:
			// TODO: Implement
		case kMsgSaveFilePanel:
			fSaveFilePanel->Show();
			break;
		case kMsgAbout:
			AboutRequested();
			break;
		case kMsgStatusUpdate:
			_StatusTextUpdate(message);
			break;
		case B_REFS_RECEIVED:
		case B_SIMPLE_DATA:
			_MessageDropped(message);
			break;

		default:
			BWindow::MessageReceived(message);
			break;
	}
}


void
MainWindow::_MessageDropped(BMessage* message)
{
	status_t status = B_MESSAGE_NOT_UNDERSTOOD;
	bool hasRef = false;

	entry_ref ref;
	if (message->FindRef("refs", &ref) != B_OK) {
		const void* data;
		ssize_t size;
		if (message->FindData("text/plain", B_MIME_TYPE, &data,
				&size) == B_OK) {
			printf("text/plain %s\n", (const char*)data);
			//fMainView->SetText((const char*)data);
		} else
			return;
	} else {
		printf("ref = %s\n", ref.name);
		BEntry entry(&ref);
		if (entry.IsDirectory()) {
			BMessage* msg = new BMessage(kMsgOpenFolder);
			msg->AddRef("prefix", &ref);
			be_app->PostMessage(msg);
		} else {
			entry_ref parent;
			if (entry.GetParent(&entry) == B_OK
				&& entry.GetRef(&parent) == B_OK)
				fSaveFilePanel->SetPanelDirectory(&parent);
			status = fMainView->Load(ref);
			//if (status == B_OK)
			//	be_roster->AddToRecentDocuments(&ref, kSignature);
			hasRef = true;
		}
	}

	if (status < B_OK) {
		char buffer[1024];
		if (hasRef) {
			snprintf(buffer, sizeof(buffer),
				"Could not open \"%s\":\n%s\n", ref.name,
				strerror(status));
		} else {
			snprintf(buffer, sizeof(buffer),
				"Could not set text:\n%s\n",
				strerror(status));
		}

		(new BAlert("Microbe request",
			buffer, "OK", NULL, NULL,
			B_WIDTH_AS_USUAL, B_STOP_ALERT))->Go();
	}
}


void
MainWindow::_StatusTextUpdate(BMessage* message)
{

	const char* statusText;
	if (fStatusText && message->FindString("text", &statusText) == B_OK) {
		fStatusText->SetText(statusText);
	}

	uint32 delayMilli;
	if (fStatusRight && message->FindUInt32("delay", &delayMilli) == B_OK) {
		char buffer[34];
		snprintf(buffer, sizeof(buffer) - 1, "%" B_PRIu32 "ms", delayMilli);
		fStatusRight->SetText(buffer);
	}
}
