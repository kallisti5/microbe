# Microbe

*A programming microorganism* for Haiku.

## Features

* Native Haiku application
* Add-on based plugin system

## Compiling

```
scons
```

## License

Released under the terms of the MIT license.
